﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using System.Threading;
using System.Globalization;
namespace googleMaptest2
{
    // this attribute can let interface call the function of javascript
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public partial class FormModifierPdi : Form
    {
        private FormAjouterMedia formAjouterMedia;
        private Pdi tmpPdi;
        public ApplicationGestion AppGest { set; get; }
        public bool Modifie { set; get; }
        public RestAPI RestAPI { set; get; }
        public Pdi Pdi { set; get; }

        public FormModifierPdi()
        {
            // Globalization
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            InitializeComponent();
            formAjouterMedia = new FormAjouterMedia();
            // initial map from html
            webBrowser1.DocumentText = Resource1.map_regulate;
            webBrowser1.ObjectForScripting = this;
        }

        // call the function of javascript from HTML(map)
        private void myInvokeScript(string name, params object[] args)
        {
            webBrowser1.Document.InvokeScript(name, args);
        }

        // when the small map is initialized, it will load the marker to add
        public void loadMarkerTA()
        {
            if(Pdi != null)
                myInvokeScript("initMarkerToAdd", Pdi.latitude, Pdi.longitude);
        }

        // Interface gets "Marker to add"'s location  
        public void getMarkerTA_Location(double latitude, double longitude)
        {
            textPdiLatitude.Text = latitude.ToString();
            textPdiLongitude.Text = longitude.ToString();
        }

        // load media to list of media
        private void ajouterUnMediaListView(Media media)
        {
            ListViewItem item = new ListViewItem(media.idMedia.ToString());
            switch (media.nature)
            {
                case 1:
                    item.SubItems.Add("Réference");
                    break;
                case 2:
                    item.SubItems.Add("Vidéo");
                    break;
                case 3:
                    item.SubItems.Add("Image");
                    break;
                case 4:
                    item.SubItems.Add("Image entête");
                    break;
                case 5:
                    item.SubItems.Add("Audio");
                    break;
                case 6:
                    item.SubItems.Add("Vidéo intégré");
                    break;
            }
            item.SubItems.Add(media.lien);
            item.SubItems.Add(media.description);
            
            listMedia.Items.Add(item);
        }

        // load medias
        private void chargerListMedia()
        {
            listMedia.Items.Clear();
            foreach (var media in Pdi.LiensMedia)
            {
                ajouterUnMediaListView(media.Value);
            }
        }

        // load all the types 
        private void chargerTypes()
        {
            
            int i;
            var typesArr = AppGest.Types.Values.ToArray();
            comboType.Items.AddRange(typesArr);
            comboType.Sorted = true;
            i = 0;
            foreach (Type type in comboType.Items)
            {
                if (Pdi.idType == type.idType)
                    break;
                i++;
            }
            comboType.SelectedIndex = i;
        }

   
        private void FormModifierPdi_Load(object sender, EventArgs e)
        {
            // is modified ?
            Modifie = false;

            comboType.Items.Clear(); 
            textPdiNom.Text = Pdi.nom;
            textPdiLatitude.Text = Pdi.latitude.ToString();
            textPdiLongitude.Text = Pdi.longitude.ToString();
            textPdiAltitude.Text = Pdi.altitude.ToString();
            richTextPdiTexte.Text = Pdi.description;
            checkBoxAffichage.Checked = Pdi.affichage;

            chargerTypes();
            //affiche les medias sur le listMedia
            chargerListMedia();

            

            //affiche le modificateur
            labelModificateur.Text = "Dernière mise à jour: " + AppGest.Comptes[Pdi.idCompte].prenom + "(" + AppGest.Comptes[Pdi.idCompte].username + ")" + ", date: " + Pdi.date;

            //affiche le markerTA sur la carte
            loadMarkerTA();
        }

        // 'delete media' button is clicked
        private void supprimerMedia_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(listMedia.SelectedItems[0].SubItems[0].Text);
            if (RestAPI.oterUnMedia(id))
            {
                listMedia.Items.Remove(listMedia.SelectedItems[0]);
                Pdi.LiensMedia.Remove(id);
            }
        }

        // 'add media' button is clicked
        private void buttonAjouterMedia_Click(object sender, EventArgs e)
        {
            formAjouterMedia.RestAPI = RestAPI;
            formAjouterMedia.Pdi = Pdi;
            formAjouterMedia.ShowDialog();
            if (formAjouterMedia.Media != null)
            {
                ajouterUnMediaListView(formAjouterMedia.Media);
            }
        }

        // 'modify pdi' button is clicked
        private void buttonModifierPdi_Click(object sender, EventArgs e)
        {
            // check inputs
            if (textPdiNom.Text.Length == 0)
            {
                MessageBox.Show("Le longueur de 'nom' doit être supérieur à 0.");
            }
            else
            {
                string nom = textPdiNom.Text;
                double latitude = Double.Parse(textPdiLatitude.Text);
                double longitude = Double.Parse(textPdiLongitude.Text);
                double altitude = Double.Parse(textPdiAltitude.Text);
                string texte = richTextPdiTexte.Text;
                bool affichage = checkBoxAffichage.Checked;
                Compte modificateur = AppGest.User;
                Type type = (Type)comboType.SelectedItem;
                string date = "";

                tmpPdi = new Pdi(Pdi.idPdi, nom, latitude, longitude, altitude, texte, affichage, modificateur.idCompte, type.idType, date);

                
                if (RestAPI.modifierUnPdi(tmpPdi))
                {
                    tmpPdi = RestAPI.chargerUnPdi(tmpPdi.idPdi);
                    Pdi.nom = tmpPdi.nom;
                    Pdi.latitude = tmpPdi.latitude;
                    Pdi.longitude = tmpPdi.longitude;
                    Pdi.altitude = tmpPdi.altitude;
                    Pdi.description = tmpPdi.description;
                    Pdi.affichage = tmpPdi.affichage;
                    Pdi.idCompte = tmpPdi.idCompte;
                    Pdi.idType = tmpPdi.idType;
                    Pdi.date = tmpPdi.date;


                    Modifie = true;
                    this.Hide();

                }

            }
        }
    }
}
