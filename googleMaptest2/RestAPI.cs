﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace googleMaptest2
{
    public class RestAPI
    {
        // api 
        const string BaseUrl = "http://project.stupidfatcat.com/projet40api/v1/";

        // login request
        public UserKey Login(string username, string password)
        {
            var request = new RestRequest(Method.POST);
            request.Resource = "login";
            request.AddParameter("username", username);
            request.AddParameter("password", password);
            
            var client = new RestClient();
            client.BaseUrl = new Uri(BaseUrl);

            var response = client.Execute<UserKey>(request);
            if (response.ErrorException != null)
            {
                const string message = "Error retrieving response.  Check inner details for more info.";
                var twilioException = new ApplicationException(message, response.ErrorException);
                throw twilioException;
            }
            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                return null;
            return response.Data;

        }

        // 'get' request
        public T GetExecute<T>(RestRequest request) where T : new()
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(BaseUrl);

            request.AddParameter("apiKey", FormMain.UserKey.apiKey);
            var response = client.Execute<T>(request);
            if (response.ErrorException != null)
            {
                const string message = "Error retrieving response.  Check inner details for more info.";
                var twilioException = new ApplicationException(message, response.ErrorException);
                throw twilioException;
            }
            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest || response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                return default(T);
            return response.Data;

        }

        // 'post' request
        public int PostExecute(RestRequest request)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(BaseUrl);

            request.AddParameter("apiKey", FormMain.UserKey.apiKey);
            var response = client.Execute<CreatedID>(request);
            if (response.ErrorException != null)
            {
                const string message = "Error retrieving response.  Check inner details for more info.";
                var twilioException = new ApplicationException(message, response.ErrorException);
                throw twilioException;
            }
            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest || response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                return -1;
            return response.Data.idCreate;
        }

        public bool BoolExecute(RestRequest request)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(BaseUrl);

            request.AddParameter("apiKey", FormMain.UserKey.apiKey);
            var response = client.Execute(request);
            if (response.ErrorException != null)
            {
                const string message = "Error retrieving response.  Check inner details for more info.";
                var twilioException = new ApplicationException(message, response.ErrorException);
                throw twilioException;
            }
            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest || response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                return false;
            return true;
        }


/*-------------------------- Create ----------------------------*/

        public Compte ajouterUnCompte(string nom, string prenom, string fonction, string username, string password)
        {
            var request = new RestRequest(Method.POST);
            request.Resource = "user";

            request.AddParameter("nom", nom);
            request.AddParameter("prenom", prenom);
            request.AddParameter("fonction", fonction);
            request.AddParameter("username", username);
            request.AddParameter("password", password);

            int id = PostExecute(request);

            if (id != -1)
                return new Compte(id, nom, prenom, fonction, username, password);
            else
                return null;
        }


        public Type ajouterUnType(string titre)
        {
            var request = new RestRequest(Method.POST);
            request.Resource = "type";

            request.AddParameter("titre", titre);

            int id = PostExecute(request);

            if (id != -1)
                return chargerUnType(id);
            else
                return null;
        }


        public Media ajouterUnMedia(int nature, string lien, int idPdi, string description)
        {
            var request = new RestRequest(Method.POST);
            request.Resource = "media";

            request.AddParameter("nature", nature);
            request.AddParameter("lien", lien);
            request.AddParameter("idPdi", idPdi);
            request.AddParameter("description", description);

            int id = PostExecute(request);

            if (id != -1)
                return new Media(id, lien, nature, idPdi, description);
            else
                return null;

        }

        public Pdi ajouterUnPdi(string nom, double latitude, double longitude, double altitude, string description, bool affichage, int idCompte, int idType)
        {
            var request = new RestRequest(Method.POST);
            request.Resource = "poi";

            request.AddParameter("nom", nom);
            request.AddParameter("latitude", latitude);
            request.AddParameter("longitude", longitude);
            request.AddParameter("altitude", altitude);
            request.AddParameter("description", description);
            request.AddParameter("affichage", affichage ? 1 : 0);
            request.AddParameter("idCompte", idCompte);
            request.AddParameter("idType", idType);

            int id = PostExecute(request);

            if (id != -1)
                return chargerUnPdi(id);
            else
                return null;
        }

/*-------------------------- Get -------------------------*/

        public Type chargerUnType(int idType)
        {
            var request = new RestRequest(Method.GET);
            request.Resource = "type/" + idType;

            return GetExecute<Type>(request);
            
        }

        public Pdi chargerUnPdi(int idPdi)
        {
            var request = new RestRequest(Method.GET);
            request.Resource = "poi/" + idPdi;

            
            Pdi pdi = GetExecute<Pdi>(request);
            if (pdi != default(Pdi))
            {
                return pdi;
            }
            else
            {
                return null;
            }
            
        }

        public Dictionary<int, Compte> chargerComptes()
        {
            var request = new RestRequest(Method.GET);
            request.Resource = "users";

            var list = GetExecute<ListResponse>(request);
            if (list != default(ListResponse))
            {
                Dictionary<int, Compte> comptes = new Dictionary<int,Compte>();
                list.users.ForEach(delegate(Compte compte)
                {
                    comptes.Add(compte.idCompte, compte);
                });
                return comptes;
            }
            else
            {
                return null;
            }
        }

        
        public Dictionary<int, Type> chargerTypes()
        {
            var request = new RestRequest(Method.GET);
            request.Resource = "types";

            var list = GetExecute<ListResponse>(request);
            if (list != default(ListResponse))
            {
                Dictionary<int, Type> types = new Dictionary<int, Type>();
                list.types.ForEach(delegate(Type type)
                {
                    types.Add(type.idType, type);
                });
                return types;
            }
            else
            {
                return null;
            }
        }

        
        public Dictionary<int, Media> chargerMedias()
        {
            var request = new RestRequest(Method.GET);
            request.Resource = "medias";

            var list = GetExecute<ListResponse>(request);
            if (list != default(ListResponse))
            {
                Dictionary<int, Media> medias = new Dictionary<int, Media>();
                list.medias.ForEach(delegate(Media media)
                {
                    medias.Add(media.idMedia, media);
                });
                return medias;
            }
            else
            {
                return null;
            }
        }

        public Dictionary<int, Pdi> chargerPdis()
        {
            var request = new RestRequest(Method.GET);
            request.Resource = "pois";

            var list = GetExecute<ListResponse>(request);
            if (list != default(ListResponse))
            {
                Dictionary<int, Pdi> pdis = new Dictionary<int, Pdi>();
                list.pois.ForEach(delegate(Pdi pdi)
                {
                    pdis.Add(pdi.idPdi, pdi);
                });
                return pdis;
            }
            else
            {
                return null;
            }
        }
        
/*---------------------------- Update -----------------------------*/
        public bool modifierUnCompte(Compte compte)
        {
            var request = new RestRequest(Method.PUT);
            request.Resource = "user/" + compte.idCompte;

            request.AddParameter("nom", compte.nom);
            request.AddParameter("prenom", compte.prenom);
            request.AddParameter("fonction", compte.fonction);
            request.AddParameter("username", compte.username);
            request.AddParameter("password", compte.password);

            return BoolExecute(request);
        }

        public bool modifierUnPdi(Pdi pdi)
        {
            var request = new RestRequest(Method.PUT);
            request.Resource = "poi/" + pdi.idPdi;

            request.AddParameter("nom", pdi.nom);
            request.AddParameter("latitude", pdi.latitude);
            request.AddParameter("longitude", pdi.longitude);
            request.AddParameter("altitude", pdi.altitude);
            request.AddParameter("description", pdi.description);
            request.AddParameter("affichage", pdi.affichage ? 1 : 0);
            request.AddParameter("idCompte", pdi.idCompte);
            request.AddParameter("idType", pdi.idType);

            return BoolExecute(request);
        }

        public bool modifierUnType(Type type)
        {
            var request = new RestRequest(Method.PUT);
            request.Resource = "type/" + type.idType;

            request.AddParameter("idType", type.idType);
            request.AddParameter("titre", type.titre);

            return BoolExecute(request);

        }

/*------------------------- delete ------------------------*/
        public bool oterUnCompte(int id)
        {
            var request = new RestRequest(Method.DELETE);
            request.Resource = "user/" + id;

            return BoolExecute(request);

        }

        public bool oterUnPdi(int id)
        {
            var request = new RestRequest(Method.DELETE);
            request.Resource = "poi/" + id;

            return BoolExecute(request);
        }

        public bool oterUnMedia(int id)
        {
            var request = new RestRequest(Method.DELETE);
            request.Resource = "media/" + id;

            return BoolExecute(request);
        }

        public bool oterUnType(int id)
        {
            var request = new RestRequest(Method.DELETE);
            request.Resource = "type/" + id;

            return BoolExecute(request);
        }
    }
}
