﻿namespace googleMaptest2
{
    partial class FormAjouterMedia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textLien = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboNature = new System.Windows.Forms.ComboBox();
            this.buttonAjouterMedia = new System.Windows.Forms.Button();
            this.textDescription = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nature";
            // 
            // textLien
            // 
            this.textLien.Location = new System.Drawing.Point(83, 8);
            this.textLien.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textLien.Name = "textLien";
            this.textLien.Size = new System.Drawing.Size(371, 23);
            this.textLien.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lien";
            // 
            // comboNature
            // 
            this.comboNature.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboNature.FormattingEnabled = true;
            this.comboNature.Location = new System.Drawing.Point(83, 72);
            this.comboNature.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboNature.Name = "comboNature";
            this.comboNature.Size = new System.Drawing.Size(186, 23);
            this.comboNature.TabIndex = 3;
            // 
            // buttonAjouterMedia
            // 
            this.buttonAjouterMedia.Location = new System.Drawing.Point(367, 68);
            this.buttonAjouterMedia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonAjouterMedia.Name = "buttonAjouterMedia";
            this.buttonAjouterMedia.Size = new System.Drawing.Size(87, 29);
            this.buttonAjouterMedia.TabIndex = 4;
            this.buttonAjouterMedia.Text = "Ajouter";
            this.buttonAjouterMedia.UseVisualStyleBackColor = true;
            this.buttonAjouterMedia.Click += new System.EventHandler(this.buttonAjouterMedia_Click);
            // 
            // textDescription
            // 
            this.textDescription.Location = new System.Drawing.Point(83, 38);
            this.textDescription.Name = "textDescription";
            this.textDescription.Size = new System.Drawing.Size(371, 23);
            this.textDescription.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Description";
            // 
            // FormAjouterMedia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 108);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textDescription);
            this.Controls.Add(this.buttonAjouterMedia);
            this.Controls.Add(this.comboNature);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textLien);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormAjouterMedia";
            this.Text = "FormAjouterMedia";
            this.Load += new System.EventHandler(this.FormAjouterMedia_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textLien;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboNature;
        private System.Windows.Forms.Button buttonAjouterMedia;
        private System.Windows.Forms.TextBox textDescription;
        private System.Windows.Forms.Label label3;
    }
}