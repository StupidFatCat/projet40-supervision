﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace googleMaptest2
{
    public class Type
    {
        public int idType { get; set; }
        public string titre { get; set; }
        public string date { get; set; }

        public Type() { }

        public Type(int id, string titre)
        {
            this.idType = id;
            this.titre = titre;
        }

        public override string ToString()
        {
            return titre;
        }
    }
}
