﻿namespace googleMaptest2
{
    partial class FormModifierPdi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textPdiNom = new System.Windows.Forms.TextBox();
            this.textPdiLongitude = new System.Windows.Forms.TextBox();
            this.textPdiAltitude = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.richTextPdiTexte = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.listMedia = new System.Windows.Forms.ListView();
            this.mediaId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mediaNature = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mediaLien = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mediaDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.supprimerMedia = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonAjouterMedia = new System.Windows.Forms.Button();
            this.buttonModifierPdi = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textPdiLatitude = new System.Windows.Forms.TextBox();
            this.labelModificateur = new System.Windows.Forms.Label();
            this.checkBoxAffichage = new System.Windows.Forms.CheckBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.comboType = new System.Windows.Forms.ComboBox();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textPdiNom
            // 
            this.textPdiNom.Location = new System.Drawing.Point(134, 14);
            this.textPdiNom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textPdiNom.Name = "textPdiNom";
            this.textPdiNom.Size = new System.Drawing.Size(140, 23);
            this.textPdiNom.TabIndex = 1;
            // 
            // textPdiLongitude
            // 
            this.textPdiLongitude.Location = new System.Drawing.Point(134, 81);
            this.textPdiLongitude.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textPdiLongitude.Name = "textPdiLongitude";
            this.textPdiLongitude.Size = new System.Drawing.Size(140, 23);
            this.textPdiLongitude.TabIndex = 2;
            // 
            // textPdiAltitude
            // 
            this.textPdiAltitude.Location = new System.Drawing.Point(134, 115);
            this.textPdiAltitude.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textPdiAltitude.Name = "textPdiAltitude";
            this.textPdiAltitude.Size = new System.Drawing.Size(140, 23);
            this.textPdiAltitude.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nom";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Longitude";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(71, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "Altitude";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(87, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "Type";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(53, 277);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 15);
            this.label6.TabIndex = 11;
            this.label6.Text = "Description";
            // 
            // richTextPdiTexte
            // 
            this.richTextPdiTexte.Location = new System.Drawing.Point(134, 274);
            this.richTextPdiTexte.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextPdiTexte.Name = "richTextPdiTexte";
            this.richTextPdiTexte.Size = new System.Drawing.Size(493, 109);
            this.richTextPdiTexte.TabIndex = 12;
            this.richTextPdiTexte.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(80, 391);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "Media";
            // 
            // listMedia
            // 
            this.listMedia.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.mediaId,
            this.mediaNature,
            this.mediaLien,
            this.mediaDescription});
            this.listMedia.ContextMenuStrip = this.contextMenuStrip1;
            this.listMedia.FullRowSelect = true;
            this.listMedia.Location = new System.Drawing.Point(134, 391);
            this.listMedia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listMedia.Name = "listMedia";
            this.listMedia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.listMedia.Size = new System.Drawing.Size(493, 120);
            this.listMedia.TabIndex = 15;
            this.listMedia.UseCompatibleStateImageBehavior = false;
            this.listMedia.View = System.Windows.Forms.View.Details;
            // 
            // mediaId
            // 
            this.mediaId.Text = "id";
            // 
            // mediaNature
            // 
            this.mediaNature.Text = "nature";
            // 
            // mediaLien
            // 
            this.mediaLien.Text = "lien";
            this.mediaLien.Width = 169;
            // 
            // mediaDescription
            // 
            this.mediaDescription.Text = "description";
            this.mediaDescription.Width = 200;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.supprimerMedia});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(130, 26);
            // 
            // supprimerMedia
            // 
            this.supprimerMedia.Name = "supprimerMedia";
            this.supprimerMedia.Size = new System.Drawing.Size(129, 22);
            this.supprimerMedia.Text = "Supprimer";
            this.supprimerMedia.Click += new System.EventHandler(this.supprimerMedia_Click);
            // 
            // buttonAjouterMedia
            // 
            this.buttonAjouterMedia.Location = new System.Drawing.Point(12, 444);
            this.buttonAjouterMedia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonAjouterMedia.Name = "buttonAjouterMedia";
            this.buttonAjouterMedia.Size = new System.Drawing.Size(108, 29);
            this.buttonAjouterMedia.TabIndex = 16;
            this.buttonAjouterMedia.Text = "ajouter media";
            this.buttonAjouterMedia.UseVisualStyleBackColor = true;
            this.buttonAjouterMedia.Click += new System.EventHandler(this.buttonAjouterMedia_Click);
            // 
            // buttonModifierPdi
            // 
            this.buttonModifierPdi.Location = new System.Drawing.Point(33, 516);
            this.buttonModifierPdi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonModifierPdi.Name = "buttonModifierPdi";
            this.buttonModifierPdi.Size = new System.Drawing.Size(87, 29);
            this.buttonModifierPdi.TabIndex = 17;
            this.buttonModifierPdi.Text = "Modifier";
            this.buttonModifierPdi.UseVisualStyleBackColor = true;
            this.buttonModifierPdi.Click += new System.EventHandler(this.buttonModifierPdi_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 15);
            this.label1.TabIndex = 19;
            this.label1.Text = "Latitude";
            // 
            // textPdiLatitude
            // 
            this.textPdiLatitude.Location = new System.Drawing.Point(134, 47);
            this.textPdiLatitude.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textPdiLatitude.Name = "textPdiLatitude";
            this.textPdiLatitude.Size = new System.Drawing.Size(140, 23);
            this.textPdiLatitude.TabIndex = 20;
            // 
            // labelModificateur
            // 
            this.labelModificateur.AutoSize = true;
            this.labelModificateur.Location = new System.Drawing.Point(132, 530);
            this.labelModificateur.Name = "labelModificateur";
            this.labelModificateur.Size = new System.Drawing.Size(100, 15);
            this.labelModificateur.TabIndex = 21;
            this.labelModificateur.Text = "labelModificateur";
            // 
            // checkBoxAffichage
            // 
            this.checkBoxAffichage.AutoSize = true;
            this.checkBoxAffichage.Location = new System.Drawing.Point(135, 233);
            this.checkBoxAffichage.Name = "checkBoxAffichage";
            this.checkBoxAffichage.Size = new System.Drawing.Size(77, 19);
            this.checkBoxAffichage.TabIndex = 22;
            this.checkBoxAffichage.Text = "Affichage";
            this.checkBoxAffichage.UseVisualStyleBackColor = true;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(305, 12);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(322, 238);
            this.webBrowser1.TabIndex = 23;
            // 
            // comboType
            // 
            this.comboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboType.FormattingEnabled = true;
            this.comboType.Location = new System.Drawing.Point(135, 149);
            this.comboType.Name = "comboType";
            this.comboType.Size = new System.Drawing.Size(139, 23);
            this.comboType.TabIndex = 24;
            // 
            // FormModifierPdi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 568);
            this.Controls.Add(this.comboType);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.checkBoxAffichage);
            this.Controls.Add(this.labelModificateur);
            this.Controls.Add(this.textPdiLatitude);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonModifierPdi);
            this.Controls.Add(this.buttonAjouterMedia);
            this.Controls.Add(this.listMedia);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.richTextPdiTexte);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textPdiAltitude);
            this.Controls.Add(this.textPdiLongitude);
            this.Controls.Add(this.textPdiNom);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormModifierPdi";
            this.Text = "Modification d\'un point d\'intérêt";
            this.Load += new System.EventHandler(this.FormModifierPdi_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textPdiNom;
        private System.Windows.Forms.TextBox textPdiLongitude;
        private System.Windows.Forms.TextBox textPdiAltitude;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox richTextPdiTexte;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListView listMedia;
        private System.Windows.Forms.ColumnHeader mediaId;
        private System.Windows.Forms.ColumnHeader mediaNature;
        private System.Windows.Forms.ColumnHeader mediaLien;
        private System.Windows.Forms.Button buttonAjouterMedia;
        private System.Windows.Forms.Button buttonModifierPdi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textPdiLatitude;
        private System.Windows.Forms.Label labelModificateur;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem supprimerMedia;
        private System.Windows.Forms.CheckBox checkBoxAffichage;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.ComboBox comboType;
        private System.Windows.Forms.ColumnHeader mediaDescription;
    }
}