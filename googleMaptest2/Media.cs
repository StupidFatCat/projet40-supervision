﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace googleMaptest2
{
    public class Media
    {
        public int idMedia { set; get; }
        public string lien { set; get; }
        public int nature { set; get; }
        public int idPdi { set; get; }
        public string description { set; get; }

        public Media() { }

        public Media(int idMedia, string lien, int nature, int idPdi, string description)
        {
            this.idMedia = idMedia;
            this.lien = lien;
            this.nature = nature;
            this.idPdi = idPdi;
            this.description = description;
        }
    }
}
