﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace googleMaptest2
{
    // adapte the library 'RestSharp'
    class ListResponse
    {
        public List<Compte> users { set; get; }
        public List<Pdi> pois { set; get; }
        public List<Type> types { set; get; }
        public List<Media> medias { set; get; }
    }
}
