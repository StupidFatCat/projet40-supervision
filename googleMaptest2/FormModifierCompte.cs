﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace googleMaptest2
{
    public partial class FormModifierCompte : Form
    {
        private Compte tmpCompte;
        public Compte Compte { set; get; }
        public bool Modifie { set; get; }
        public RestAPI RestAPI { set; get; }
        public FormModifierCompte()
        {
            InitializeComponent();
        }

        private void FormModifierCompte_Load(object sender, EventArgs e)
        {
            // load account
            Modifie = false;
            textCompteNom.Text = Compte.nom;
            textComptePrenom.Text = Compte.prenom;
            textCompteFonction.Text = Compte.fonction;
            textCompteUsername.Text = Compte.username;
        }

        // 'modify account' button is clicked
        private void buttonModifierCompte_Click(object sender, EventArgs e)
        {
            // check input
            if (textCompteUsername.Text.Length < 3 || textComptePassword.Text.Length < 6 && textComptePassword.Text.Length > 0)
            {
                MessageBox.Show("Le longueur de 'username' doit être supérieur à 2 et celui de 'mot de passe doit être supérieur à 5");
            }
            // if password is empty, the password will not be modified
            else
            {
                string nom = textCompteNom.Text;
                string prenom = textComptePrenom.Text;
                string fonction = textCompteFonction.Text;
                string username = textCompteUsername.Text;
                string password = textComptePassword.Text;
                tmpCompte = new Compte(Compte.idCompte, nom, prenom, fonction, username, password);
                if (RestAPI.modifierUnCompte(tmpCompte))
                {
                    Compte.nom = tmpCompte.nom;
                    Compte.prenom = tmpCompte.prenom;
                    Compte.fonction = tmpCompte.fonction;
                    Compte.username = tmpCompte.username;
                    Compte.password = tmpCompte.password;
                    Modifie = true;
                    this.Hide();
                }
            }
        }
    }
}
