﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace googleMaptest2
{
    public partial class FormAjouterMedia : Form
    {
        private string[] natures = { "Référence", "Vidéo", "Image", "Image entête", "Audio", "Vidéo intégré" };
        public Media Media { set; get; }
        public Pdi Pdi { set; get; }
        public RestAPI RestAPI { set; get; }
        public FormAjouterMedia()
        {
            InitializeComponent();
            // initialize natures of Media
            comboNature.Items.AddRange(natures);
        }

        private void FormAjouterMedia_Load(object sender, EventArgs e)
        {
            // reset
            Media = null;
            textLien.Text = "";
            textDescription.Text = "";
            comboNature.SelectedIndex = -1;
        }

        // 'add a media' button is clicked
        private void buttonAjouterMedia_Click(object sender, EventArgs e)
        {
            int nature = -1;
            if (textLien.Text.Length == 0 && textDescription.Text.Length == 0)
            {
                MessageBox.Show("Les lien et description ne peuvent pas être vide.");
            }
            else
            {
                nature = comboNature.SelectedIndex + 1;
                Media = RestAPI.ajouterUnMedia(nature, textLien.Text, Pdi.idPdi, textDescription.Text);
                if (Media != null)
                {
                    Pdi.ajouterUnMedia(Media);
                    this.Hide();

                }
                else
                {
                    MessageBox.Show("Echec d'ajout du media");
                }
                
            }
        }
    }
}
