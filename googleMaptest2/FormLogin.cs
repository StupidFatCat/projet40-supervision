﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace googleMaptest2
{
    public partial class FormLogin : Form
    {
        private RestAPI restAPI;
        public FormLogin()
        {
            InitializeComponent();
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            // when FormLogin load
            // initialize RESTful API controller
            restAPI = new RestAPI();
        }

        // login button
        private void button1_Click(object sender, EventArgs e)
        {
            UserKey userKey = restAPI.Login(textUsername.Text, textPass.Text);
            if (userKey == null)
                MessageBox.Show("Invalid username/password");
            else
            {
                // success
                FormMain formMain = new FormMain();
                FormMain.UserKey = userKey;
                formMain.RestAPI = restAPI;
                this.Hide();
                formMain.ShowDialog();
                this.Close();
            }
        }

    }
}
