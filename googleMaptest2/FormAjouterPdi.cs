﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using System.Threading;
using System.Globalization;
namespace googleMaptest2
{
    // this attribute can let interface call the function of javascript
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public partial class FormAjouterPdi : Form
    {
        public RestAPI RestAPI { set; get; }
        public ApplicationGestion AppGest;
        public string Lat { set; get; }
        public string Lng { set; get; }
        public Pdi Pdi { set; get; }
        public FormAjouterPdi()
        {
            // Globalization
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            InitializeComponent();
            Lat = "";
            Lng = "";
            // initial map from html
            webBrowser1.DocumentText = Resource1.map_regulate;
            webBrowser1.ObjectForScripting = this;
        }

        // call the function of javascript from HTML(map)
        private void myInvokeScript(string name, params object[] args)
        {
            webBrowser1.Document.InvokeScript(name, args);
        }

        // when the small map is initialized, it will load the marker to add
        public void loadMarkerTA()
        {
            if (Lat.Length > 0 && Lng.Length > 0)
                myInvokeScript("initMarkerToAdd", double.Parse(Lat), double.Parse(Lng));
        }

        // Interface gets "Marker to add"'s location  
        public void getMarkerTA_Location(double latitude, double longitude)
        {
            textPdiLatitude.Text = latitude.ToString();
            textPdiLongitude.Text = longitude.ToString();
        }

        // load all the types 
        private void chargerTypes()
        {
            var typesArr = AppGest.Types.Values.ToArray();
            comboType.Items.AddRange(typesArr);
            comboType.Sorted = true;
        }

        private void FormAjouterPdi_Load(object sender, EventArgs e)
        {
            // initialze
            Pdi = null;
            comboType.Items.Clear();
            textPdiLatitude.Text = Lat;
            textPdiLongitude.Text = Lng;
            textPdiAltitude.Text = "0";
            textPdiNom.Text = "";
            richTextPdiTexte.Text = "";
            checkBoxAffichage.Checked = true;
            chargerTypes();
            comboType.SelectedIndex = -1;
            loadMarkerTA();
        }

        // 'add a pdi' button is clicked
        private void buttonAjouterPdiForm_Click(object sender, EventArgs e)
        {
            // check inputs
            if (textPdiNom.Text.Length == 0 || comboType.SelectedItem == null)
            {
                MessageBox.Show("Le nom/type ne peut pas être vide.");
            }
            else
            {
                // get inputs
                string nom = textPdiNom.Text;
                double latitude = double.Parse(textPdiLatitude.Text);
                double longtitude = double.Parse(textPdiLongitude.Text);
                double altitude = double.Parse(textPdiAltitude.Text);
                string description = richTextPdiTexte.Text;
                bool affichage = checkBoxAffichage.Checked;
                Compte modificateur = AppGest.User;
                Type type = (Type)comboType.SelectedItem;
                
                // call api
                Pdi = RestAPI.ajouterUnPdi(nom, latitude, longtitude, altitude, description, affichage, modificateur.idCompte, type.idType);
                if (Pdi != null)
                {
                    AppGest.ajouterUnPdi(Pdi);
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Echec d'ajout du Pdi.");
                }
            }
        }

    }
}
