﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace googleMaptest2
{
    public class Compte
    {
        public int idCompte { set; get; }
        public string nom { set; get; }
        public string prenom { set; get; }
        public string fonction { set; get; }
        public string username { set; get; }
        public string password { set; get; }

        public Compte() { }

        public Compte(int id, string nom, string prenom, string fonction, string username, string password)
        {
            this.idCompte = id;
            this.nom = nom;
            this.prenom = prenom;
            this.fonction = fonction;
            this.username = username;
            this.password = password;
        }

    }
}
