﻿namespace googleMaptest2
{
    partial class FormModifierType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonModifierType = new System.Windows.Forms.Button();
            this.textTypeModifier = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonModifierType
            // 
            this.buttonModifierType.Location = new System.Drawing.Point(182, 12);
            this.buttonModifierType.Name = "buttonModifierType";
            this.buttonModifierType.Size = new System.Drawing.Size(75, 23);
            this.buttonModifierType.TabIndex = 0;
            this.buttonModifierType.Text = "Modifier";
            this.buttonModifierType.UseVisualStyleBackColor = true;
            this.buttonModifierType.Click += new System.EventHandler(this.buttonModifierType_Click);
            // 
            // textTypeModifier
            // 
            this.textTypeModifier.Location = new System.Drawing.Point(12, 12);
            this.textTypeModifier.Name = "textTypeModifier";
            this.textTypeModifier.Size = new System.Drawing.Size(164, 21);
            this.textTypeModifier.TabIndex = 1;
            // 
            // FormModifierType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 45);
            this.Controls.Add(this.textTypeModifier);
            this.Controls.Add(this.buttonModifierType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormModifierType";
            this.Text = "Modifier Type";
            this.Load += new System.EventHandler(this.FormModifierType_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonModifierType;
        private System.Windows.Forms.TextBox textTypeModifier;
    }
}