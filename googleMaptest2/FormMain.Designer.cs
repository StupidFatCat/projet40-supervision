﻿namespace googleMaptest2
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPdi = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonAjouterPdi = new System.Windows.Forms.Button();
            this.textLongitude = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textLatitude = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.listPdi = new System.Windows.Forms.ListView();
            this.idPdi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.nomPdi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.latitude = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.longitude = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.altitude = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.modifier_Pdi = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimer_Pdi = new System.Windows.Forms.ToolStripMenuItem();
            this.tabType = new System.Windows.Forms.TabPage();
            this.textTitre = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.listType = new System.Windows.Forms.ListView();
            this.idType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Titre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.modifier_Type = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimer_Type = new System.Windows.Forms.ToolStripMenuItem();
            this.tabCompte = new System.Windows.Forms.TabPage();
            this.listCompte = new System.Windows.Forms.ListView();
            this.columnID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnNom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPrenom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnFonction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnUsername = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.modifier_Compte = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimer_Compte = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textComptePassword = new System.Windows.Forms.TextBox();
            this.textCompteUsername = new System.Windows.Forms.TextBox();
            this.textCompteFonction = new System.Windows.Forms.TextBox();
            this.textComptePrenom = new System.Windows.Forms.TextBox();
            this.textCompteNom = new System.Windows.Forms.TextBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPdi.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tabType.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.tabCompte.SuspendLayout();
            this.contextMenuStrip3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.webBrowser1);
            this.splitContainer1.Size = new System.Drawing.Size(1104, 556);
            this.splitContainer1.SplitterDistance = 360;
            this.splitContainer1.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPdi);
            this.tabControl1.Controls.Add(this.tabType);
            this.tabControl1.Controls.Add(this.tabCompte);
            this.tabControl1.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(357, 553);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPdi
            // 
            this.tabPdi.Controls.Add(this.groupBox1);
            this.tabPdi.Controls.Add(this.listPdi);
            this.tabPdi.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPdi.Location = new System.Drawing.Point(4, 24);
            this.tabPdi.Name = "tabPdi";
            this.tabPdi.Padding = new System.Windows.Forms.Padding(3);
            this.tabPdi.Size = new System.Drawing.Size(349, 525);
            this.tabPdi.TabIndex = 0;
            this.tabPdi.Text = "Points d\'intérêt";
            this.tabPdi.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonAjouterPdi);
            this.groupBox1.Controls.Add(this.textLongitude);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textLatitude);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(11, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(332, 96);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Gestion";
            // 
            // buttonAjouterPdi
            // 
            this.buttonAjouterPdi.Location = new System.Drawing.Point(101, 66);
            this.buttonAjouterPdi.Name = "buttonAjouterPdi";
            this.buttonAjouterPdi.Size = new System.Drawing.Size(103, 23);
            this.buttonAjouterPdi.TabIndex = 4;
            this.buttonAjouterPdi.Text = "Ajouter un point";
            this.buttonAjouterPdi.UseVisualStyleBackColor = true;
            this.buttonAjouterPdi.Click += new System.EventHandler(this.buttonAjouterPdi_Click);
            // 
            // textLongitude
            // 
            this.textLongitude.Enabled = false;
            this.textLongitude.Location = new System.Drawing.Point(178, 37);
            this.textLongitude.Name = "textLongitude";
            this.textLongitude.Size = new System.Drawing.Size(114, 23);
            this.textLongitude.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(202, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "longitude";
            // 
            // textLatitude
            // 
            this.textLatitude.Enabled = false;
            this.textLatitude.Location = new System.Drawing.Point(19, 37);
            this.textLatitude.Name = "textLatitude";
            this.textLatitude.Size = new System.Drawing.Size(114, 23);
            this.textLatitude.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "latitude";
            // 
            // listPdi
            // 
            this.listPdi.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idPdi,
            this.nomPdi,
            this.latitude,
            this.longitude,
            this.altitude});
            this.listPdi.ContextMenuStrip = this.contextMenuStrip1;
            this.listPdi.FullRowSelect = true;
            this.listPdi.GridLines = true;
            this.listPdi.Location = new System.Drawing.Point(11, 117);
            this.listPdi.Name = "listPdi";
            this.listPdi.Size = new System.Drawing.Size(332, 400);
            this.listPdi.TabIndex = 1;
            this.listPdi.UseCompatibleStateImageBehavior = false;
            this.listPdi.View = System.Windows.Forms.View.Details;
            this.listPdi.Click += new System.EventHandler(this.listPdi_Click);
            this.listPdi.DoubleClick += new System.EventHandler(this.modifier_Pdi_Click);
            // 
            // idPdi
            // 
            this.idPdi.Text = "id";
            this.idPdi.Width = 38;
            // 
            // nomPdi
            // 
            this.nomPdi.Text = "nom";
            // 
            // latitude
            // 
            this.latitude.Text = "Latitude";
            this.latitude.Width = 81;
            // 
            // longitude
            // 
            this.longitude.Text = "Longitude";
            this.longitude.Width = 81;
            // 
            // altitude
            // 
            this.altitude.Text = "Altitude";
            this.altitude.Width = 74;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modifier_Pdi,
            this.supprimer_Pdi});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(130, 48);
            // 
            // modifier_Pdi
            // 
            this.modifier_Pdi.Name = "modifier_Pdi";
            this.modifier_Pdi.Size = new System.Drawing.Size(129, 22);
            this.modifier_Pdi.Text = "Modifier";
            this.modifier_Pdi.Click += new System.EventHandler(this.modifier_Pdi_Click);
            // 
            // supprimer_Pdi
            // 
            this.supprimer_Pdi.Name = "supprimer_Pdi";
            this.supprimer_Pdi.Size = new System.Drawing.Size(129, 22);
            this.supprimer_Pdi.Text = "Supprimer";
            this.supprimer_Pdi.Click += new System.EventHandler(this.supprimer_Pdi_Click);
            // 
            // tabType
            // 
            this.tabType.Controls.Add(this.textTitre);
            this.tabType.Controls.Add(this.button3);
            this.tabType.Controls.Add(this.listType);
            this.tabType.Location = new System.Drawing.Point(4, 24);
            this.tabType.Name = "tabType";
            this.tabType.Padding = new System.Windows.Forms.Padding(3);
            this.tabType.Size = new System.Drawing.Size(349, 525);
            this.tabType.TabIndex = 1;
            this.tabType.Text = "Type";
            this.tabType.UseVisualStyleBackColor = true;
            // 
            // textTitre
            // 
            this.textTitre.Location = new System.Drawing.Point(6, 4);
            this.textTitre.Name = "textTitre";
            this.textTitre.Size = new System.Drawing.Size(256, 23);
            this.textTitre.TabIndex = 2;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(268, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "ajouter";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.buttonAjouterUnType_Click);
            // 
            // listType
            // 
            this.listType.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idType,
            this.Titre});
            this.listType.ContextMenuStrip = this.contextMenuStrip2;
            this.listType.FullRowSelect = true;
            this.listType.GridLines = true;
            this.listType.Location = new System.Drawing.Point(3, 32);
            this.listType.Name = "listType";
            this.listType.Size = new System.Drawing.Size(343, 490);
            this.listType.TabIndex = 0;
            this.listType.UseCompatibleStateImageBehavior = false;
            this.listType.View = System.Windows.Forms.View.Details;
            this.listType.DoubleClick += new System.EventHandler(this.modifier_Type_Click);
            // 
            // idType
            // 
            this.idType.Text = "ID";
            this.idType.Width = 73;
            // 
            // Titre
            // 
            this.Titre.Text = "Titre";
            this.Titre.Width = 246;
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modifier_Type,
            this.supprimer_Type});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(130, 48);
            // 
            // modifier_Type
            // 
            this.modifier_Type.Name = "modifier_Type";
            this.modifier_Type.Size = new System.Drawing.Size(129, 22);
            this.modifier_Type.Text = "Modifier";
            this.modifier_Type.Click += new System.EventHandler(this.modifier_Type_Click);
            // 
            // supprimer_Type
            // 
            this.supprimer_Type.Name = "supprimer_Type";
            this.supprimer_Type.Size = new System.Drawing.Size(129, 22);
            this.supprimer_Type.Text = "Supprimer";
            this.supprimer_Type.Click += new System.EventHandler(this.supprimer_Type_Click);
            // 
            // tabCompte
            // 
            this.tabCompte.Controls.Add(this.listCompte);
            this.tabCompte.Controls.Add(this.groupBox3);
            this.tabCompte.Location = new System.Drawing.Point(4, 24);
            this.tabCompte.Name = "tabCompte";
            this.tabCompte.Padding = new System.Windows.Forms.Padding(3);
            this.tabCompte.Size = new System.Drawing.Size(349, 525);
            this.tabCompte.TabIndex = 2;
            this.tabCompte.Text = "Compte";
            this.tabCompte.UseVisualStyleBackColor = true;
            // 
            // listCompte
            // 
            this.listCompte.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnID,
            this.columnNom,
            this.columnPrenom,
            this.columnFonction,
            this.columnUsername});
            this.listCompte.ContextMenuStrip = this.contextMenuStrip3;
            this.listCompte.FullRowSelect = true;
            this.listCompte.GridLines = true;
            this.listCompte.Location = new System.Drawing.Point(0, 209);
            this.listCompte.Name = "listCompte";
            this.listCompte.Size = new System.Drawing.Size(349, 317);
            this.listCompte.TabIndex = 0;
            this.listCompte.UseCompatibleStateImageBehavior = false;
            this.listCompte.View = System.Windows.Forms.View.Details;
            this.listCompte.DoubleClick += new System.EventHandler(this.modifier_Compte_Click);
            // 
            // columnID
            // 
            this.columnID.Text = "ID";
            // 
            // columnNom
            // 
            this.columnNom.Text = "Nom";
            // 
            // columnPrenom
            // 
            this.columnPrenom.Text = "Prenom";
            // 
            // columnFonction
            // 
            this.columnFonction.Text = "Fonction";
            // 
            // columnUsername
            // 
            this.columnUsername.Text = "Username";
            // 
            // contextMenuStrip3
            // 
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modifier_Compte,
            this.supprimer_Compte});
            this.contextMenuStrip3.Name = "contextMenuStrip3";
            this.contextMenuStrip3.Size = new System.Drawing.Size(130, 48);
            // 
            // modifier_Compte
            // 
            this.modifier_Compte.Name = "modifier_Compte";
            this.modifier_Compte.Size = new System.Drawing.Size(129, 22);
            this.modifier_Compte.Text = "Modifier";
            this.modifier_Compte.Click += new System.EventHandler(this.modifier_Compte_Click);
            // 
            // supprimer_Compte
            // 
            this.supprimer_Compte.Name = "supprimer_Compte";
            this.supprimer_Compte.Size = new System.Drawing.Size(129, 22);
            this.supprimer_Compte.Text = "Supprimer";
            this.supprimer_Compte.Click += new System.EventHandler(this.supprimer_Compte_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.textComptePassword);
            this.groupBox3.Controls.Add(this.textCompteUsername);
            this.groupBox3.Controls.Add(this.textCompteFonction);
            this.groupBox3.Controls.Add(this.textComptePrenom);
            this.groupBox3.Controls.Add(this.textCompteNom);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(337, 197);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Compte";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(28, 168);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "Ajouter";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.buttonAjouterUnCompte_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 15);
            this.label8.TabIndex = 1;
            this.label8.Text = "Mot de passe";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "Nom d\'utilisateur";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 15);
            this.label6.TabIndex = 1;
            this.label6.Text = "Fonction";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Prénom";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(69, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "Nom";
            // 
            // textComptePassword
            // 
            this.textComptePassword.Location = new System.Drawing.Point(109, 143);
            this.textComptePassword.Name = "textComptePassword";
            this.textComptePassword.Size = new System.Drawing.Size(222, 23);
            this.textComptePassword.TabIndex = 0;
            // 
            // textCompteUsername
            // 
            this.textCompteUsername.Location = new System.Drawing.Point(109, 114);
            this.textCompteUsername.Name = "textCompteUsername";
            this.textCompteUsername.Size = new System.Drawing.Size(222, 23);
            this.textCompteUsername.TabIndex = 0;
            // 
            // textCompteFonction
            // 
            this.textCompteFonction.Location = new System.Drawing.Point(109, 85);
            this.textCompteFonction.Name = "textCompteFonction";
            this.textCompteFonction.Size = new System.Drawing.Size(222, 23);
            this.textCompteFonction.TabIndex = 0;
            // 
            // textComptePrenom
            // 
            this.textComptePrenom.Location = new System.Drawing.Point(109, 56);
            this.textComptePrenom.Name = "textComptePrenom";
            this.textComptePrenom.Size = new System.Drawing.Size(222, 23);
            this.textComptePrenom.TabIndex = 0;
            // 
            // textCompteNom
            // 
            this.textCompteNom.Location = new System.Drawing.Point(109, 27);
            this.textCompteNom.Name = "textCompteNom";
            this.textCompteNom.Size = new System.Drawing.Size(222, 23);
            this.textCompteNom.TabIndex = 0;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(740, 556);
            this.webBrowser1.TabIndex = 0;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 556);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormMain";
            this.Text = "Interface";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPdi.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabType.ResumeLayout(false);
            this.tabType.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.tabCompte.ResumeLayout(false);
            this.contextMenuStrip3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.ListView listPdi;
        private System.Windows.Forms.ColumnHeader idPdi;
        private System.Windows.Forms.ColumnHeader nomPdi;
        private System.Windows.Forms.ColumnHeader latitude;
        private System.Windows.Forms.ColumnHeader longitude;
        private System.Windows.Forms.ColumnHeader altitude;
        private System.Windows.Forms.Button buttonAjouterPdi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textLongitude;
        private System.Windows.Forms.TextBox textLatitude;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPdi;
        private System.Windows.Forms.TabPage tabType;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem modifier_Pdi;
        private System.Windows.Forms.ToolStripMenuItem supprimer_Pdi;
        private System.Windows.Forms.TabPage tabCompte;
        private System.Windows.Forms.ListView listType;
        private System.Windows.Forms.ColumnHeader idType;
        private System.Windows.Forms.ColumnHeader Titre;
        private System.Windows.Forms.TextBox textTitre;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem modifier_Type;
        private System.Windows.Forms.ToolStripMenuItem supprimer_Type;
        private System.Windows.Forms.ListView listCompte;
        private System.Windows.Forms.ColumnHeader columnID;
        private System.Windows.Forms.ColumnHeader columnNom;
        private System.Windows.Forms.ColumnHeader columnPrenom;
        private System.Windows.Forms.ColumnHeader columnFonction;
        private System.Windows.Forms.ColumnHeader columnUsername;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip3;
        private System.Windows.Forms.ToolStripMenuItem modifier_Compte;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ToolStripMenuItem supprimer_Compte;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textComptePassword;
        private System.Windows.Forms.TextBox textCompteUsername;
        private System.Windows.Forms.TextBox textCompteFonction;
        private System.Windows.Forms.TextBox textComptePrenom;
        private System.Windows.Forms.TextBox textCompteNom;
    }
}

