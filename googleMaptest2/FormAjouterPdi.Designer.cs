﻿namespace googleMaptest2
{
    partial class FormAjouterPdi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxAffichage = new System.Windows.Forms.CheckBox();
            this.textPdiLatitude = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textPdiAltitude = new System.Windows.Forms.TextBox();
            this.textPdiLongitude = new System.Windows.Forms.TextBox();
            this.textPdiNom = new System.Windows.Forms.TextBox();
            this.richTextPdiTexte = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonAjouterPdiForm = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.comboType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // checkBoxAffichage
            // 
            this.checkBoxAffichage.AutoSize = true;
            this.checkBoxAffichage.Location = new System.Drawing.Point(122, 176);
            this.checkBoxAffichage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBoxAffichage.Name = "checkBoxAffichage";
            this.checkBoxAffichage.Size = new System.Drawing.Size(77, 19);
            this.checkBoxAffichage.TabIndex = 33;
            this.checkBoxAffichage.Text = "Affichage";
            this.checkBoxAffichage.UseVisualStyleBackColor = true;
            // 
            // textPdiLatitude
            // 
            this.textPdiLatitude.Location = new System.Drawing.Point(122, 47);
            this.textPdiLatitude.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.textPdiLatitude.Name = "textPdiLatitude";
            this.textPdiLatitude.Size = new System.Drawing.Size(163, 23);
            this.textPdiLatitude.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 15);
            this.label1.TabIndex = 31;
            this.label1.Text = "Latitude";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(79, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 15);
            this.label5.TabIndex = 29;
            this.label5.Text = "Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(63, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 15);
            this.label4.TabIndex = 28;
            this.label4.Text = "Altitude";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.TabIndex = 27;
            this.label3.Text = "Longitude";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 15);
            this.label2.TabIndex = 26;
            this.label2.Text = "Nom";
            // 
            // textPdiAltitude
            // 
            this.textPdiAltitude.Location = new System.Drawing.Point(122, 113);
            this.textPdiAltitude.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.textPdiAltitude.Name = "textPdiAltitude";
            this.textPdiAltitude.Size = new System.Drawing.Size(163, 23);
            this.textPdiAltitude.TabIndex = 25;
            // 
            // textPdiLongitude
            // 
            this.textPdiLongitude.Location = new System.Drawing.Point(122, 80);
            this.textPdiLongitude.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.textPdiLongitude.Name = "textPdiLongitude";
            this.textPdiLongitude.Size = new System.Drawing.Size(163, 23);
            this.textPdiLongitude.TabIndex = 24;
            // 
            // textPdiNom
            // 
            this.textPdiNom.Location = new System.Drawing.Point(122, 14);
            this.textPdiNom.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.textPdiNom.Name = "textPdiNom";
            this.textPdiNom.Size = new System.Drawing.Size(163, 23);
            this.textPdiNom.TabIndex = 23;
            // 
            // richTextPdiTexte
            // 
            this.richTextPdiTexte.Location = new System.Drawing.Point(122, 277);
            this.richTextPdiTexte.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.richTextPdiTexte.Name = "richTextPdiTexte";
            this.richTextPdiTexte.Size = new System.Drawing.Size(511, 135);
            this.richTextPdiTexte.TabIndex = 35;
            this.richTextPdiTexte.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 280);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 15);
            this.label6.TabIndex = 34;
            this.label6.Text = "Description";
            // 
            // buttonAjouterPdiForm
            // 
            this.buttonAjouterPdiForm.Location = new System.Drawing.Point(22, 383);
            this.buttonAjouterPdiForm.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonAjouterPdiForm.Name = "buttonAjouterPdiForm";
            this.buttonAjouterPdiForm.Size = new System.Drawing.Size(87, 29);
            this.buttonAjouterPdiForm.TabIndex = 36;
            this.buttonAjouterPdiForm.Text = "Ajouter";
            this.buttonAjouterPdiForm.UseVisualStyleBackColor = true;
            this.buttonAjouterPdiForm.Click += new System.EventHandler(this.buttonAjouterPdiForm_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(309, 12);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(324, 256);
            this.webBrowser1.TabIndex = 37;
            // 
            // comboType
            // 
            this.comboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboType.FormattingEnabled = true;
            this.comboType.Location = new System.Drawing.Point(122, 146);
            this.comboType.Name = "comboType";
            this.comboType.Size = new System.Drawing.Size(163, 23);
            this.comboType.TabIndex = 38;
            // 
            // FormAjouterPdi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 433);
            this.Controls.Add(this.comboType);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.buttonAjouterPdiForm);
            this.Controls.Add(this.richTextPdiTexte);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.checkBoxAffichage);
            this.Controls.Add(this.textPdiLatitude);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textPdiAltitude);
            this.Controls.Add(this.textPdiLongitude);
            this.Controls.Add(this.textPdiNom);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormAjouterPdi";
            this.Text = "Ajout d\'un point d\'intérêt";
            this.Load += new System.EventHandler(this.FormAjouterPdi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxAffichage;
        private System.Windows.Forms.TextBox textPdiLatitude;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textPdiAltitude;
        private System.Windows.Forms.TextBox textPdiLongitude;
        private System.Windows.Forms.TextBox textPdiNom;
        private System.Windows.Forms.RichTextBox richTextPdiTexte;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonAjouterPdiForm;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.ComboBox comboType;
    }
}