﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace googleMaptest2
{
    public class Pdi
    {

        public int idPdi { set; get; }
        public string nom { set; get; }
        public double latitude { set; get; }
        public double longitude { set; get; }
        public double altitude { set; get; }
        public string description { set; get; }
        public bool affichage { set; get; }
        public int idCompte { set; get; }
        public int idType { set; get; }
        public string date { set; get; }

        public Dictionary<int, Media> LiensMedia { set; get; }
        public Pdi() 
        {
            this.LiensMedia = new Dictionary<int, Media>();
        }
        public Pdi(int id, string nom, double latitude, double longitude, double altitude, string description, bool affichage, int idCompte, int idType, string date)
        {
            this.idPdi = id;
            this.nom = nom;
            this.latitude = latitude;
            this.longitude = longitude;
            this.altitude = altitude;
            this.description = description;
            this.affichage = affichage;
            this.idCompte = idCompte;
            this.idType = idType;
            this.date = date;


            this.LiensMedia = new Dictionary<int, Media>();
        }
      

        public int ajouterUnMedia(Media media) {
            this.LiensMedia.Add(media.idMedia, media);
            return 0;
        }


        public int oterUnMedia(int id)
        {
            this.LiensMedia.Remove(id);
            return 0;
        }

    }
}
