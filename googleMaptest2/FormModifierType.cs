﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace googleMaptest2
{
    public partial class FormModifierType : Form
    {
        private Type tmpType;
        public Type Type { set; get; }
        public RestAPI RestAPI { set; get; }
        public bool Modifie { set; get; }
        public FormModifierType()
        {
            InitializeComponent();
        }

        private void FormModifierType_Load(object sender, EventArgs e)
        {
            Modifie = false;
            textTypeModifier.Text = Type.titre;
        }

        // 'modify type' button is clicked
        private void buttonModifierType_Click(object sender, EventArgs e)
        {
            string titre = textTypeModifier.Text;
            tmpType = new Type(Type.idType, titre);
            if (RestAPI.modifierUnType(tmpType))
            {
                tmpType = RestAPI.chargerUnType(tmpType.idType);
                Type.titre = tmpType.titre;
                Type.date = tmpType.date;
                Modifie = true;
                this.Hide();
            }
        }
    }
}
