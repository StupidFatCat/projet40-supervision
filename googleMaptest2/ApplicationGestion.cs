﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace googleMaptest2
{
    public class ApplicationGestion
    {
        public Dictionary<int, Type> Types {set; get;}
        public Dictionary<int, Pdi> Pdis {set; get;}
        public Dictionary<int, Compte> Comptes {set; get;}
        public Compte User { set; get; }



        public void ajouterUnType(Type type)
        {
            Types.Add(type.idType, type);
        }

        public void ajouterUnPdi(Pdi pdi) 
        {
            Pdis.Add(pdi.idPdi, pdi);
        }

        public void ajouterUnCompte(Compte compte) 
        {
            Comptes.Add(compte.idCompte, compte);
        }

        public void oterUnType(int id)
        {
            Types.Remove(id);
        }
        
        public void oterUnPdi(int id)
        {
            Types.Remove(id);
        }

        public void oterUnCompte(int id)
        {
            Comptes.Remove(id);
        }

        public void ajouterUnMedia(Media media)
        {
            Pdis[media.idPdi].ajouterUnMedia(media);
        }
    }
}
