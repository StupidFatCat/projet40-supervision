﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using System.Threading;
using System.Globalization;

namespace googleMaptest2
{
    // this attribute can let interface call the function of javascript
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public partial class FormMain : Form
    {
        private ApplicationGestion appGest;
        private FormModifierType formModifierType;
        private FormModifierCompte formModifierCompte;
        private FormModifierPdi formModifierPdi;
        private FormAjouterPdi formAjouterPdi;
        public static UserKey UserKey;
        public RestAPI RestAPI { set; get; }
        public FormMain()
        {
            // Globalization
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            InitializeComponent();
            // initial map from html(Embeded in project)
            webBrowser1.DocumentText = Resource1.map;
            webBrowser1.ObjectForScripting = this;
        }

        // Interface gets "Marker to add"'s location  
        public void getMarkerTA_Location(double latitude, double longitude)
        {
            textLatitude.Text = latitude.ToString();
            textLongitude.Text = longitude.ToString();
        }

        // call the function of javascript from HTML(map)
        private void myInvokeScript(string name, params object[] args)
        {
            webBrowser1.Document.InvokeScript(name, args);
        }

        // when map is initialized, it will load all the markers
        public void loadMarkers()
        {
            foreach (var pdi in appGest.Pdis)
            {
                int id = pdi.Value.idPdi;
                string nom = pdi.Value.nom;
                double latitude = pdi.Value.latitude;
                double longitude = pdi.Value.longitude;
                bool affichage = pdi.Value.affichage;
                myInvokeScript("loadMarker", id, nom, latitude, longitude, affichage);
            }
        }

        // load all the pdis to the list of pdi
        private void chargerListViewPdi()
        {
            foreach (var pdi in appGest.Pdis)
            {
                ajouterUnPdiListView(pdi.Value);
            }
        }

        // load all the types to the list of type 
        private void chargerListViewType()
        {
            foreach (var type in appGest.Types)
            {
                ajouterUnTypeListView(type.Value);
            }
        }


        // load all the accounts ot the list of account
        private void chargerListViewCompte()
        {
            foreach (var compte in appGest.Comptes)
            {
                ajouterUnComptListView(compte.Value);
            }
        }

        // add an account to the list of account
        private void ajouterUnComptListView(Compte compte)
        {
                ListViewItem item = new ListViewItem(compte.idCompte.ToString());
                item.SubItems.Add(compte.nom.ToString());
                item.SubItems.Add(compte.prenom.ToString());
                item.SubItems.Add(compte.fonction.ToString());
                item.SubItems.Add(compte.username.ToString());
                listCompte.Items.Add(item);
        }

        // add a type to the list of type
        private void ajouterUnTypeListView(Type type)
        {
            ListViewItem item = new ListViewItem(type.idType.ToString());
            item.SubItems.Add(type.titre.ToString());
            listType.Items.Add(item);
        }

        // add a pdi to the list of pdi
        private void ajouterUnPdiListView(Pdi pdi)
        {
            ListViewItem item = new ListViewItem(pdi.idPdi.ToString());
            item.SubItems.Add(pdi.nom.ToString());
            item.SubItems.Add(pdi.latitude.ToString());
            item.SubItems.Add(pdi.longitude.ToString());
            item.SubItems.Add(pdi.altitude.ToString());
            listPdi.Items.Add(item);

            //ajouter sur la carte
            myInvokeScript("loadMarker", pdi.idPdi, pdi.nom, pdi.latitude, pdi.longitude, pdi.affichage);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            // initialize the sub forms
            formModifierType = new FormModifierType();
            formModifierCompte = new FormModifierCompte();
            formModifierPdi = new FormModifierPdi();
            formAjouterPdi = new FormAjouterPdi();

            // load data from web server 
            appGest = new ApplicationGestion();
            appGest.Comptes = RestAPI.chargerComptes();
            appGest.Types = RestAPI.chargerTypes();
            appGest.Pdis = RestAPI.chargerPdis();
            var medias = RestAPI.chargerMedias();
            foreach (var media in medias.Values)
                appGest.ajouterUnMedia(media);
            // define who is now using interface
            appGest.User = appGest.Comptes[UserKey.idCompte];

            // load to list
            this.chargerListViewPdi();
            this.chargerListViewType();
            this.chargerListViewCompte();


        }

        // 'delete' button is clicked
        private void supprimer_Pdi_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(listPdi.SelectedItems[0].SubItems[0].Text);
            if (RestAPI.oterUnPdi(id))
            {
                listPdi.Items.Remove(listPdi.SelectedItems[0]);
                appGest.Pdis.Remove(id);
                myInvokeScript("deleteMarker", id);
            }
        }

        // 'add a type' button is clicked
        private void buttonAjouterUnType_Click(object sender, EventArgs e)
        {
            //add a type
            if (textTitre.Text.Length == 0)
            {
                MessageBox.Show("Le titre ne peut pas être vide.");
            }
            else
            {
                Type type = RestAPI.ajouterUnType(textTitre.Text);
                if (type != null)
                {
                    appGest.ajouterUnType(type); 
                    ajouterUnTypeListView(type);
                }
                
            }
        }

        // 'modify type' button is clicked
        private void modifier_Type_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(listType.SelectedItems[0].SubItems[0].Text);
            //formModifierType.Type = Connect.chargerUnType(id);
            formModifierType.Type = appGest.Types[id];
            formModifierType.RestAPI = RestAPI;
            formModifierType.ShowDialog();

            if (formModifierType.Modifie == true)
                listType.SelectedItems[0].SubItems[1].Text = formModifierType.Type.titre.ToString();
        }

        // 'delete type' button is clicked
        private void supprimer_Type_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(listType.SelectedItems[0].SubItems[0].Text);
            if (RestAPI.oterUnType(id))
            {
                listType.Items.Remove(listType.SelectedItems[0]);
                appGest.Types.Remove(id);
            }
        }

        // 'add an account' button is clicked
        private void buttonAjouterUnCompte_Click(object sender, EventArgs e)
        {
            //verifier les inputs
            if (textCompteUsername.Text.Length < 3 || textComptePassword.Text.Length < 6)
            {
                MessageBox.Show("Le longueur de 'username' doit être supérieur à 2 et celui de 'mot de passe doit être supérieur à 5");
            }
            else
            {
                string nom, prenom, fonction, username, password;
                nom = textCompteNom.Text;
                prenom = textComptePrenom.Text;
                fonction = textCompteFonction.Text;
                username = textCompteUsername.Text;
                password = textComptePassword.Text;
                Compte compte = RestAPI.ajouterUnCompte(nom, prenom, fonction, username, password);
                if (compte != null)
                {
                    appGest.ajouterUnCompte(compte);
                    ajouterUnComptListView(compte);
                }
            }
        }

        // 'modify account' button is clicked
        private void modifier_Compte_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(listCompte.SelectedItems[0].SubItems[0].Text);
            //formModifierCompte.Compte = Connect.chargerUnCompte(id);
            formModifierCompte.Compte = appGest.Comptes[id];
            formModifierCompte.RestAPI = RestAPI;
            formModifierCompte.ShowDialog();
            if (formModifierCompte.Modifie == true)
            {
                Compte compte = formModifierCompte.Compte;
                listCompte.SelectedItems[0].SubItems[1].Text = compte.nom.ToString();
                listCompte.SelectedItems[0].SubItems[2].Text = compte.prenom.ToString();
                listCompte.SelectedItems[0].SubItems[3].Text = compte.fonction.ToString();
                listCompte.SelectedItems[0].SubItems[4].Text = compte.username.ToString();
            }
        }

        // 'delete account' button is clicked
        private void supprimer_Compte_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(listCompte.SelectedItems[0].SubItems[0].Text);
            if (RestAPI.oterUnCompte(id))
            {
                listCompte.Items.Remove(listCompte.SelectedItems[0]);
                appGest.Comptes.Remove(id);
            }
            
        }

        // 'modify pdi' button is clicked
        private void modifier_Pdi_Click(object sender, EventArgs e)
        {
            
            int id = Int32.Parse(listPdi.SelectedItems[0].SubItems[0].Text);
            //formModifierPdi.Pdi = Connect.chargerUnPdi(id);
            formModifierPdi.Pdi = appGest.Pdis[id];
            formModifierPdi.RestAPI = RestAPI;
            
            formModifierPdi.AppGest = appGest;
            formModifierPdi.ShowDialog();

            if (formModifierPdi.Modifie == true)
            {
                Pdi pdi = formModifierPdi.Pdi;
                listPdi.SelectedItems[0].SubItems[1].Text = pdi.nom;
                listPdi.SelectedItems[0].SubItems[2].Text = pdi.latitude.ToString();
                listPdi.SelectedItems[0].SubItems[3].Text = pdi.longitude.ToString();
                listPdi.SelectedItems[0].SubItems[4].Text = pdi.altitude.ToString();

                //modifier sur la carte
                myInvokeScript("modifyMarker", pdi.idPdi, pdi.nom, pdi.latitude, pdi.longitude, pdi.affichage);

            }

            
        }

        // 'add a pdi' button is clicked
        private void buttonAjouterPdi_Click(object sender, EventArgs e)
        {
            formAjouterPdi.RestAPI = RestAPI;
            formAjouterPdi.AppGest = appGest;
            formAjouterPdi.Lat = textLatitude.Text;
            formAjouterPdi.Lng = textLongitude.Text;
            formAjouterPdi.ShowDialog();
            if (formAjouterPdi.Pdi != null)
            {
                ajouterUnPdiListView(formAjouterPdi.Pdi);
            }

        }


        // 'a line of list pdi' is clicked, the marker bounces (animation)
        private void listPdi_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(listPdi.SelectedItems[0].SubItems[0].Text);
            myInvokeScript("toggleBounce", id);
        }

       
    }
}
