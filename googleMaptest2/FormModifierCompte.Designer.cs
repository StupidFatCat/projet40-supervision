﻿namespace googleMaptest2
{
    partial class FormModifierCompte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textComptePassword = new System.Windows.Forms.TextBox();
            this.textCompteUsername = new System.Windows.Forms.TextBox();
            this.textCompteFonction = new System.Windows.Forms.TextBox();
            this.textComptePrenom = new System.Windows.Forms.TextBox();
            this.textCompteNom = new System.Windows.Forms.TextBox();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.textComptePassword);
            this.groupBox3.Controls.Add(this.textCompteUsername);
            this.groupBox3.Controls.Add(this.textCompteFonction);
            this.groupBox3.Controls.Add(this.textComptePrenom);
            this.groupBox3.Controls.Add(this.textCompteNom);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(3, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(337, 197);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Compte";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(28, 168);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "Modifier";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.buttonModifierCompte_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 15);
            this.label8.TabIndex = 1;
            this.label8.Text = "Mot de passe";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "Username";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 15);
            this.label6.TabIndex = 1;
            this.label6.Text = "Fonction";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Prénom";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(69, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "Nom";
            // 
            // textComptePassword
            // 
            this.textComptePassword.Location = new System.Drawing.Point(109, 143);
            this.textComptePassword.Name = "textComptePassword";
            this.textComptePassword.Size = new System.Drawing.Size(222, 23);
            this.textComptePassword.TabIndex = 0;
            // 
            // textCompteUsername
            // 
            this.textCompteUsername.Location = new System.Drawing.Point(109, 114);
            this.textCompteUsername.Name = "textCompteUsername";
            this.textCompteUsername.Size = new System.Drawing.Size(222, 23);
            this.textCompteUsername.TabIndex = 0;
            // 
            // textCompteFonction
            // 
            this.textCompteFonction.Location = new System.Drawing.Point(109, 85);
            this.textCompteFonction.Name = "textCompteFonction";
            this.textCompteFonction.Size = new System.Drawing.Size(222, 23);
            this.textCompteFonction.TabIndex = 0;
            // 
            // textComptePrenom
            // 
            this.textComptePrenom.Location = new System.Drawing.Point(109, 56);
            this.textComptePrenom.Name = "textComptePrenom";
            this.textComptePrenom.Size = new System.Drawing.Size(222, 23);
            this.textComptePrenom.TabIndex = 0;
            // 
            // textCompteNom
            // 
            this.textCompteNom.Location = new System.Drawing.Point(109, 27);
            this.textCompteNom.Name = "textCompteNom";
            this.textCompteNom.Size = new System.Drawing.Size(222, 23);
            this.textCompteNom.TabIndex = 0;
            // 
            // FormModifierCompte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 213);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormModifierCompte";
            this.Text = "Modifier Compte";
            this.Load += new System.EventHandler(this.FormModifierCompte_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textComptePassword;
        private System.Windows.Forms.TextBox textCompteUsername;
        private System.Windows.Forms.TextBox textCompteFonction;
        private System.Windows.Forms.TextBox textComptePrenom;
        private System.Windows.Forms.TextBox textCompteNom;
    }
}